# Distributed and Parallel Algorithms for Big Data

## Implementation of various parallel algorithms for processing data, such as:

* Hybrid Range Partitioning for Parallel Search (task1.py)
* Parallel Joins using Hash Join with Disjoint and Broadcast partitioning methods (task2.py)
* Parallel Partitioned Sort (task3.py)
* Parallel Group By (task4.py and task5.py)

## Other Files

* **FIT5148 - Assignment 1.pdf**: Assignment description
* **Assignment1_final_v1.ipynb**: Python code as Jupyter notebook
