# Summary #

Some of my programming projects during my postgraduate degree at Monash.

### Projects ###

* **Distributed and Parallel Algorithms for Big Data**
> Implementation of various parallel algorithms for processing data, such as:

> * Hybrid Range Partitioning for Parallel Search

> * Parallel Joins using Hash Join with Disjoint and Broadcast partitioning methods

> * Parallel Partitioned Sort

> * Parallel Group By

* **Parsing Text - Data Wrangling Asg 1**
> Using regular expresssions to extract necessary text from XML documents of Patent applications into CSV and JSON formats.

* **Text Preprocessing - Data Wrangling Asg 2**
> Convert PDF research papers to text, tokenize the entire corpus and preprocess the tokens to generate a vocabulary and sparse term count.


* **3-Tier Hotel Booking System**
> A distributed and parallel programming project that involved building a client , a broker and back-end server applications as part of a hotel reservation system. (_Java_)


* **Multi-threaded Sum of Squares** 

> A multi-threaded program to compute the sum of squares. (_C_)


* **Postgraduate IT Industry Experience Project**

> Repository can be viewed here: https://bitbucket.org/potato108p/potato-cyclesafe